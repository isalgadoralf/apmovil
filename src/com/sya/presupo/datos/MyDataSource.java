package com.sya.presupo.datos;

import java.text.SimpleDateFormat;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class MyDataSource {
	public SQLiteDatabase db;
	public MyHelper helper;
	public SimpleDateFormat FormatoFecha;
	public MyDataSource(Context contex) {
		// TODO Auto-generated constructor stub
		
		helper =  MyHelper.getInstance(contex);
		//FormatoFecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		FormatoFecha = new SimpleDateFormat("yyyy-MM-dd");
		
	}
	public void open(){
		db=  helper.getWritableDatabase();
		
	}
	public void openRead(){
		db=  helper.getReadableDatabase();
	}
		
	public void close(){
		helper.close();
	}
	
	
}