package com.sya.presupo.datos;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import scz.bo.spresupo.R;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sya.presupo.model.Deitpre;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Umedida;
import com.sya.presupo.spresupo.Puente;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class MyPDF {
	private Context contexto;
	private final static String NOMBRE_DIRECTORIO = "MiPdf";
	private final static String NOMBRE_DOCUMENTO = "prueba.pdf";
	private final static String ETIQUETA_ERROR = "ERROR";
	Font bfBold12 = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD,
			new BaseColor(0, 0, 0));
	Font bf12 = new Font(FontFamily.TIMES_ROMAN, 12);
	private Double total = 0.0;
	public MyPDF(Context contexto) {
		// TODO Auto-generated constructor stub
		this.contexto = contexto;
		total = 0.0;
	}

	public static File getRuta() {

		// El fichero ser� almacenado en un directorio dentro del directorio
		// Descargas
		File ruta = null;
		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			ruta = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
					NOMBRE_DIRECTORIO);

			if (ruta != null) {
				if (!ruta.mkdirs()) {
					if (!ruta.exists()) {
						return null;
					}
				}
			}
		} else {
		}

		return ruta;
	}

	public static File crearFichero(String nombreFichero) throws IOException {
		File ruta = getRuta();
		File fichero = null;
		// if (ruta != null)
		fichero = new File(ruta, nombreFichero);
		return fichero;
	}

	public void generarPDF() {
		Document documento = new Document();

		try {

			// Creamos el fichero con el nombre que deseemos.
			File f = crearFichero(NOMBRE_DOCUMENTO);

			// Creamos el flujo de datos de salida para el fichero donde
			// guardaremos el pdf.
			FileOutputStream ficheroPdf = new FileOutputStream(
					f.getAbsolutePath());

			// Asociamos el flujo que acabamos de crear al documento.
			PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);

			// Abrimos el documento.
			documento.open();
			// Insertamos una imagen que se encuentra en los recursos de la
			// aplicaci�n.
			Bitmap bitmap = BitmapFactory.decodeResource(
					this.contexto.getResources(), R.drawable.manoobra);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			Image imagen = Image.getInstance(stream.toByteArray());
			imagen.scalePercent(35);
			documento.add(imagen);

			// A�adimos un t�tulo con la fuente por defecto.
			documento.add(new Paragraph(""+new Date()));

			// A�adimos un t�tulo con una fuente personalizada.
			Font font = FontFactory.getFont(FontFactory.HELVETICA, 28,
					Font.BOLD, BaseColor.BLACK);
			documento.add(new Paragraph("", font));
			documento.add(new Paragraph("Costo de Mano Obra", font));
			documento.add(new Paragraph("   ", font));
			//documento.add(new Paragraph("   ", font));
			PdfPTable table = new PdfPTable(8);
			cargarCabecera(table);
			cargarTabla(table);
			cargarTotal(table);
			documento.add(table);
  
			// Agregar marca de agua
		/*	font = FontFactory.getFont(FontFactory.HELVETICA, 42, Font.BOLD,
					BaseColor.GRAY);
			ColumnText.showTextAligned(writer.getDirectContentUnder(),
					Element.ALIGN_CENTER, new Paragraph(
							"amatellanes.wordpress.com", font), 297.5f, 421,
					writer.getPageNumber() % 2 == 1 ? 45 : -45);*/
			Bitmap bitmap2 = BitmapFactory.decodeResource(
					this.contexto.getResources(), R.drawable.logoespada);
			ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
			bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, stream2);
			Image imagen2 = Image.getInstance(stream2.toByteArray());
			imagen2.scalePercent(15);
			imagen2.setAlignment(Element.ALIGN_CENTER);
		imagen2.setAbsolutePosition(100, 100);
			documento.add(imagen2);
		} catch (DocumentException e) {

			Log.e(ETIQUETA_ERROR, e.getMessage());

		} catch (IOException e) {

			Log.e(ETIQUETA_ERROR, e.getMessage());

		} finally {

			// Cerramos el documento.
			documento.close();
		}
	}
	private void cargarTotal(PdfPTable table) {
	
		insertCell(table, "Total Presupuesto", Element.ALIGN_RIGHT, 7, bfBold12);

		insertCell(table, ""+this.total, Element.ALIGN_RIGHT, 1, bfBold12);


	
	}

	private void cargarCabecera(PdfPTable table) {
	

		insertCell(table, "#", Element.ALIGN_LEFT, 1, bfBold12);

		insertCell(table, "Descripcion", Element.ALIGN_LEFT, 3, bfBold12);

		insertCell(table, "UMedida", Element.ALIGN_LEFT, 1, bfBold12);
		insertCell(table, "P.U.", Element.ALIGN_RIGHT, 1, bfBold12);
		insertCell(table, "Cant", Element.ALIGN_RIGHT, 1, bfBold12);
		// insertCell(table,""+de.getCantidad() , Element.ALIGN_LEFT, 1, bf12);

		insertCell(table, "Total", Element.ALIGN_RIGHT, 1, bfBold12);

		// documento.add(tabla);
	}

	private void cargarTabla(PdfPTable table) {
		// Insertamos una tabla.
		Deitpre de = new Deitpre();
		ManangerSQlite m = new ManangerSQlite(this.contexto);
		// ArrayList<Object> lista = m.getListaCampo(detalle, "presupuestoID",
		// detalle.getPresupuestoID());
		ArrayList<Object> lista = m.getListaCampo(de, "presupuestoID",
				Puente.presupuesto.getPresupuestoID());

		for (int x = 0; x < lista.size(); x++) {
			insertCell(table, "" + x, Element.ALIGN_LEFT, 1, bf12);
			de = (Deitpre) lista.get(x);
			Item it = new Item();
			it = (Item) m.getObjectCampo(it, "itemID", de.getItemID());
			insertCell(table, it.getDescripcion() + x, Element.ALIGN_LEFT, 3,
					bf12);
			Umedida um = new Umedida();
			um = (Umedida) m.getObjectCampo(um, "umedidaID", it.getUmedidaID());
			insertCell(table, um.getAbreviatura(), Element.ALIGN_LEFT, 1, bf12);
			insertCell(table, "" + it.getPrecio(), Element.ALIGN_RIGHT, 1, bf12);
			insertCell(table, "" + de.getCantidad(), Element.ALIGN_RIGHT, 1,
					bf12);
			// insertCell(table,""+de.getCantidad() , Element.ALIGN_LEFT, 1,
			// bf12);

			Double total = 0.0;
			total = de.getCantidad() * it.getPrecio();
			this.total =  this.total + total;

			insertCell(table, "" + total, Element.ALIGN_RIGHT, 1, bf12);

		}
		// documento.add(tabla);
	}

	public void showPdfFile() {
		Toast.makeText(contexto, "Leyendo documento", Toast.LENGTH_LONG).show();

		String sdCardRoot = Environment.getExternalStorageDirectory().getPath();
		File ruta = getRuta();
		File fichero = null;
		if (ruta != null)
			fichero = new File(ruta, NOMBRE_DOCUMENTO);

		File file = new File(ruta, NOMBRE_DOCUMENTO);

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			contexto.startActivity(intent);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(contexto, "No Application Available to View PDF",
					Toast.LENGTH_SHORT).show();
		}
	}

	private void insertCell(PdfPTable table, String text, int align,
			int colspan, Font font) {

		// create a new cell with the specified Text and Font
		PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
		// set the cell alignment
		cell.setHorizontalAlignment(align);
		// set the cell column span in case you want to merge two or more cells
		cell.setColspan(colspan);
		// cell.se
		// in case there is no text and you wan to create an empty row
		if (text.trim().equalsIgnoreCase("")) {
			cell.setMinimumHeight(10f);
		}
		// add the call to the table
		table.addCell(cell);

	}

}
