package com.sya.presupo.util;

import java.util.ArrayList;

import com.sya.presupo.estructuras.Point;

import android.util.Log;


public class MyMath {
	public static double distancia(Point a, Point b)
    {
    	double bax = (b.x-a.x);
    	double a2 = Math.pow(bax, 2);
    	double bay = (b.y-a.y);
    	double b2 = Math.pow(bay, 2);
    	double argumento = a2+b2;
    			
     return Math.abs( Math.sqrt(argumento));
    }
    public static double tresSimple(double piexles,double equiva, double nvpixcel){
    	double numerador = nvpixcel*equiva;
    	return numerador/piexles;
    }
    
    public static double getTotalDistancia(ArrayList<Point>points,double equi,double pix){
    	if(points.size()<2)
    	 	return 0.0;
    	double aux = 0.0;
    	int i, len;
	    len = points.size();
	    
	    for (i = 0; i < len-1; i++) {
	    	Point a = points.get(i);
	    	Point b = points.get(i+1);
	    	double xx = MyMath.distancia(a, b);
	    	double yy = MyMath.tresSimple(pix, equi, xx);
	    	Log.d("pix", ""+pix);
	    	Log.d("equi", ""+equi);
	    	
	    	aux =  aux +yy;
	    }
	    return aux;
    }
    public static double Redondear(double numero)
    {
           return Math.rint(numero*100)/100;
    }
    public static Point cpointTopoint(Point po,double equi,double pix)
    {
    		Point res = new Point();
    	     res.x =  (float)MyMath.tresSimple(pix, equi, po.x);
    	     res.y =  (float)MyMath.tresSimple(pix, equi, po.y);
    	     return res;
           
    }
    public static double calculoArea(ArrayList<Point> puntos){
    	int  n = puntos.size();
    	Point aux = puntos.get(n-1);
    	puntos.add(aux);
    	double x =  MyMath.deterX(puntos);
    	double y =  MyMath.deterY(puntos);
    	return (x-y)/2;
    }
    public static double deterX(ArrayList<Point> puntos){
    	int n = puntos.size();
    	double aux = 0.0;
    	for (int i = 0; i+1 < n; i++) {
    		Point a = puntos.get(i);
    		Point b = puntos.get(i+1);
    		aux = aux + a.x*b.y;
		}
    	return aux;
    }
    public static double deterY(ArrayList<Point> puntos){
    	int n = puntos.size();
    	double aux = 0.0;
    	for (int i = 0; i+1 < n; i++) {
    		Point a = puntos.get(i);
    		Point b = puntos.get(i+1);
    		aux = aux + a.y*b.x;
		}
    	return aux;
    }
}
