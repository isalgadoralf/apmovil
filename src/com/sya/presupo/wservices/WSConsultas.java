package com.sya.presupo.wservices;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Presupuesto;
import com.sya.presupo.model.Proveedores;

import android.util.Log;

public class WSConsultas {
	private static final String TAG = WSConsultas.class.getSimpleName();
	private static final String NAMESPACE = "http://datos/";

	// private static final String URL =
	// "http://192.168.1.101:8084/WebPedidos/consultas?xsd=1";
	//private static final String URL = "http://192.168.1.100:8084/WConsultas/consultas?xsd=1";
	//private static final String URL = "http://192.168.1.13:8080/WConsultas/consultas?xsd=1";
	//private static final String URL = "http://192.168.176.139:8080/WConsultas/consultas?xsd=1";
	//http://localhost:8080/WConsultas/consultas?wsdl	
	private static final String URL = "http://52.40.74.203:8080/WConsultas/consultas?wsdl";

	private static final String M_GET_CLIENTE = "getCliente";
	private static final String AS_GET_CLIENTE = NAMESPACE + M_GET_CLIENTE;

	private static final String M_GET_PROVEEDOR = "getProveedores";
	private static final String AS_GET_PROVEEDOR = NAMESPACE + M_GET_PROVEEDOR;

	private static final String M_GET_ITEMS = "getItems";
	private static final String AS_GET_ITEMS = NAMESPACE + M_GET_ITEMS;

	private static final String M_SET_PRESUPO = "setPresupuesto";
	private static final String AS_SET_PRESUPO = NAMESPACE + M_SET_PRESUPO;

	private static final String M_GET_PRESUPO = "getPresupuesto";
	private static final String AS_GET_PRESUPO = NAMESPACE + M_GET_PRESUPO;

	private static final String M_SET_PEDIDOS = "crearPedido";
	private static final String AS_SET_PEDIDOS = NAMESPACE + M_SET_PEDIDOS;

	private Gson mGson;

	public WSConsultas() {
		// TODO Auto-generated constructor stub
		mGson = new Gson();
	}
	
	
	

	public ArrayList<Proveedores> getProveedor(String fecha)
			throws IOException, XmlPullParserException {
		HashMap<String, Object> parametros = new HashMap<String, Object>();

		// HashMap<String, Object> parametros = new HashMap<String, Object>();
		// parametros.put("fechaactualizacion", fecha);

		SoapPrimitive result = (SoapPrimitive) consumir(AS_GET_PROVEEDOR,
				M_GET_PROVEEDOR, parametros);
		Type listType = new TypeToken<List<Proveedores>>() {
		}.getType();
		ArrayList<Proveedores> lista = mGson.fromJson(result.toString(),
				listType);
		Log.d("BES", "" + lista.size());
		return lista;
	}

	public ArrayList<Item> getItems(String fecha) throws IOException,
			XmlPullParserException {
		HashMap<String, Object> parametros = new HashMap<String, Object>();

		// HashMap<String, Object> parametros = new HashMap<String, Object>();
		// parametros.put("fechaactualizacion", fecha);

		SoapPrimitive result = (SoapPrimitive) consumir(AS_GET_ITEMS,
				M_GET_ITEMS, parametros);
		Type listType = new TypeToken<List<Item>>() {
		}.getType();
		ArrayList<Item> lista = mGson.fromJson(result.toString(), listType);
		Log.d("BES", "" + lista.size());
		return lista;
	}

	public Cliente getCliente(int nit) throws IOException,
			XmlPullParserException {

		HashMap<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("nit", nit);

		SoapPrimitive result = (SoapPrimitive) consumir(AS_GET_CLIENTE,
				M_GET_CLIENTE, parametros);

		Cliente cli = mGson.fromJson(result.toString(), Cliente.class);

		return cli;
	}

	public String getPresupuesto(int idpresu) throws IOException,
			XmlPullParserException {

		HashMap<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idpre", idpresu);

		SoapPrimitive result = (SoapPrimitive) consumir(AS_GET_PRESUPO,
				M_GET_PRESUPO, parametros);

		//Cliente cli = mGson.fromJson(result.toString(), Cliente.class);

		return result.toString();
	}

	public String guardarPresupo(Presupuesto pre) throws IOException,
			XmlPullParserException {
		Gson gsonn = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String jsonpedido = gsonn.toJson(pre);
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("pre", jsonpedido);

		SoapPrimitive result = (SoapPrimitive) consumir(AS_SET_PRESUPO,
				M_SET_PRESUPO, parametros);

		String cli = mGson.fromJson(result.toString(), String.class);
		return cli;
	}

	public String guardarPedido(Pedido pedido) throws IOException,
			XmlPullParserException {

		String jsonpedido = mGson.toJson(pedido);
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("pedido", jsonpedido);

		SoapPrimitive result = (SoapPrimitive) consumir(AS_SET_PEDIDOS,
				M_SET_PEDIDOS, parametros);

		String cli = mGson.fromJson(result.toString(), String.class);
		return cli;
	}

	protected Object consumir(String accionSoap, String metodo,
			Map<String, Object> parametros) throws IOException,
			XmlPullParserException {

		// Request
		SoapObject soapObject = new SoapObject(NAMESPACE, metodo);
		if (parametros != null) {
			for (Map.Entry<String, Object> param : parametros.entrySet()) {
				soapObject.addProperty(param.getKey(), param.getValue());
			}
		}

		// Sobre
		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);

		soapEnvelope.setOutputSoapObject(soapObject);

		// Marshal
		MarshalFloat marshalFloat = new MarshalFloat();
		marshalFloat.register(soapEnvelope);

		// Transporte
		HttpTransportSE httpTransport = new HttpTransportSE(URL);

		try {
			httpTransport.call(accionSoap, soapEnvelope);

			return soapEnvelope.getResponse();

		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			throw new IOException(
					"GenericWebService:consumir:Error de conexion con el servidor. "
							+ e.getMessage());
		} catch (XmlPullParserException e) {
			Log.e(TAG, e.getMessage());
			throw new XmlPullParserException(
					"GeneriWebService::consumir:Error al hacer el parser en la respuesta. "
							+ e.getMessage());
		}

	}

}
