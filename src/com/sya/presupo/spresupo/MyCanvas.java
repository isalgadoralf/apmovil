package com.sya.presupo.spresupo;

import java.util.ArrayList;

import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.estructuras.Linea;
import com.sya.presupo.estructuras.Point;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Umedida;
import com.sya.presupo.util.Constantes;
import com.sya.presupo.util.MyMath;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

@SuppressLint("ClickableViewAccessibility")
public class MyCanvas extends View {
	private Canvas canvas;
	final Paint paint;
	private Bitmap bitmap;
	public double equi;
	public double pix;
	public double altura;
	public eItems it;
	public ArrayList<Point> puntos = new ArrayList<Point>();
	public ArrayList<eItems> lsitem = new ArrayList<eItems>();
	public boolean isarea;
	public Path path;

	private Linea lineas;
	public int accion;
	public int index;

	
	public MyCanvas(Context context, int w, int h) {
		super(context);
		// TODO Auto-generated constructor stub
		bitmap = Constantes.resizeBitmapAnchoAlto(Puente.imagen, h, w);
		lineas = new Linea();
		accion = 0;
		index = -1;
		/*
		 * paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		 * paint.setColor(Color.WHITE); paint.setStyle(Style.FILL_AND_STROKE );
		 */
		this.paint = new Paint();
		this.paint.setAntiAlias(true);
		this.paint.setDither(true);

		this.paint.setColor(-16711936);
		this.paint.setStyle(Paint.Style.STROKE);
		this.paint.setStrokeJoin(Paint.Join.ROUND);
		this.paint.setStrokeCap(Paint.Cap.ROUND);
		this.paint.setStrokeWidth(9.0F);
		this.path = new Path();
		this.isarea = false;
		// init();

	}

	public void destroy() {
		if (bitmap != null) {
			bitmap.recycle();
		}
	}

	@Override
	public void onDraw(Canvas c) {
	
		c.drawBitmap(bitmap,
				new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()),
				new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), null);

		if (it != null) {
			// AREA EN EL PISO
			if (it.getTipo() == 2)
				poli(c);
			// DIBUAJAR LINEAS
			if (it.getTipo() == 0) {
				// drawPoly(c, puntos);
				DibujarLineas(c, lineas);
			}
			if (it.getTipo() == 1) {
				// drawPoly(c, puntos);
				DibujarPuntos(c);
			}
			} else {
			
				drawPoly(c);
			}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();
		
		
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			this.accionPresionar(x, y);
			break;
		case MotionEvent.ACTION_MOVE:
			Log.e("MOVE", "" + x + "  " + y + " " + this.accion + "  " + index);
			if (accion == 1) {
				if (index > -1) {
					Log.e("MOVE", "" + index);
					lineas.getPuntos().get(index).x = x;
					lineas.getPuntos().get(index).y = y;
					// this.notify();
					invalidate();

				}

			}

			break;
	/*	case MotionEvent.ACTION_UP:
		
			break;*/
		/*
		 * case MotionEvent.ACTION_MOVE: moveTouch(x, y); invalidate(); break;
		 * case MotionEvent.ACTION_UP: upTouch(); invalidate(); break;
		 */
		}
		return true;
	}

	private void drawPoly(Canvas canvas) {
		// line at minimum...
		if (lineas.getPuntos().size() < 2) {
			return;
		}
//
//		int i, len;
//		len = points.size();
//		for (i = 0; i < len - 1; i++) {
//			Point punto = points.get(i);
//			Point p = points.get(i + 1);
//			canvas.drawLine(punto.x, punto.y, p.x, p.y, paint);
//		}
		ArrayList<Point> p = lineas.getPuntos();
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);
		for (i = 0; i < len - 1; i++) {
			Point punto = p.get(i);
			Point pf = p.get(i + 1);
			canvas.drawLine(punto.x, punto.y, pf.x, pf.y, paint);

			canvas.drawCircle(punto.x, punto.y, this.lineas.getRadio(), paint1);
		}
		Point punto = p.get(len - 1);
		canvas.drawCircle(punto.x, punto.y, lineas.getRadio(), paint1);


	}

	public void calexquivalencia() {
		Point a = lineas.getPuntos().get(0);
		Point b = lineas.getPuntos().get(1);
		this.pix = MyMath.distancia(a, b);

	}

	public void clearCanvas() {
		this.puntos = new ArrayList<Point>();
		lineas = new Linea();
		invalidate();
	}

	public double calcularMuro() {
		double cantidad = MyMath.getTotalDistancia(lineas.getPuntos(), equi, pix);
		it.setCantidad(MyMath.Redondear(cantidad));
		return cantidad;
	}

	public void poli(Canvas canvas) {
		if (lineas.getPuntos().size() < 2) {
			return;
		}
		Paint paint = new Paint();
		paint.setColor(Color.TRANSPARENT);
		paint.setAlpha(50);
		paint.setStrokeWidth(4);
		paint.setStyle(Paint.Style.FILL);
		
		DibujarLineas(canvas, lineas);
		Path p = PointToPath();
		canvas.drawPath(p, paint);
		
		
		canvas.drawPath(path, paint);
	}
	private Path PointToPath(){
		Path path = new Path();
		int n =  lineas.getPuntos().size();
		Point p =  lineas.getPuntos().get(0);
		path.moveTo(p.x, p.y);
		for (int i = 1; i < n; i++) {
			p = lineas.getPuntos().get(i);
			path.lineTo(p.x, p.y);
		}
		//path.close();
		return path;
	}

	public void Nuevo() {

		this.clearCanvas();
		this.lsitem.clear();
		this.path = new Path();
		this.isarea = false;
		this.puntos = new ArrayList<Point>();
		this.accion = 0;
		
		lineas = new Linea();
	}

	public void DibujarLineas(Canvas canvas, Linea puntos) {
		// line at minimum...
		if (puntos.getPuntos().size() < 2) {
			return;
		}
		ArrayList<Point> p = puntos.getPuntos();
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);
		
		
		for (i = 0; i < len - 1; i++) {
			Point punto = p.get(i);
			Point pf = p.get(i + 1);
			canvas.drawLine(punto.x, punto.y, pf.x, pf.y, paint);

			canvas.drawCircle(punto.x, punto.y, puntos.getRadio(), paint1);
		}
		Point punto = p.get(len - 1);
		canvas.drawCircle(punto.x, punto.y, puntos.getRadio(), paint1);

	}
	public void DibujarPuntos(Canvas canvas) {
		// line at minimum...
		if (puntos.size() < 1) {
			return;
		}
		ArrayList<Point> p = puntos;
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);
		
		
		for (i = 0; i < len ; i++) {
			Point punto = p.get(i);
			canvas.drawCircle(punto.x, punto.y, 10, paint1);
		}
		

	}

	public void accionPresionar(float x, float y) {
		if (it == null) {
			//puntos.add(new Point(x, y));
		    if(this.accion == 0){
			lineas.getPuntos().add(new Point(x, y));
			invalidate();}
		} else {
			if (this.accion == 0) { // //////// NUEVO
				switch (it.getTipo()) {
				case 0: // LINEA
					puntos.add(new Point(x, y));
					lineas.getPuntos().add(new Point(x, y));
					Log.e("PUNTOS", "" + lineas.getPuntos().size());
					invalidate();
					break;
				case 1:// PUNTO
					puntos.add(new Point(x, y));
					lineas.getPuntos().add(new Point(x, y));
					Log.e("PUNTOS", "" + lineas.getPuntos().size());
					invalidate();
					break;

				case 2:// AREA
//					if (!isarea) {
//						
//						path.moveTo(x, y);
//						isarea = !isarea;
//
//						// puntos.add(new Point(x, y));
//					} else {
//
//						path.lineTo(x, y);
//
//						// invalidate();
//					}
					lineas.getPuntos().add(new Point(x, y));
					Point aux = new Point(x, y);
					aux = MyMath.cpointTopoint(aux, this.equi, this.pix);
					puntos.add(aux);
					invalidate();
					break;

				}
			}

		}
		if (this.accion == 1) { // //////////// MODIFICAR
			int n = lineas.getPuntos().size();
			for (int i = 0; i < n; i++) {
				double ax = x - lineas.getPuntos().get(i).x;
				double ay = y - lineas.getPuntos().get(i).y;
				float d = (float) Math.sqrt(ax * ax + ay * ay);
				// Log.e("VALOR ORGINAL",""+lineas.getPuntos().get(i).toString());
				// Log.e("valor de x", ""+x);
				// Log.e("valor de y", ""+y);
				// Log.e("valor de ax", "" + ax);
				// Log.e("valor de ay", "" + ay);
				// Log.e("DISTANCIA", "" + d);
				// Log.e("DISTANCIA", "----------");
				if (d <= lineas.getRadio()) {
					this.index = i;
					invalidate();
					Log.e("SE PUEDE", "" + index);

				}

			}
			// invalidate();
		}

		// /--------------------------------

	}
}
