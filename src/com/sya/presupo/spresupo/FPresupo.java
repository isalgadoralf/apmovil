package com.sya.presupo.spresupo;

import scz.bo.spresupo.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class FPresupo extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fpresupo);
	}

	public void onItems(View v) {

		Intent aux = new Intent(this, FlistaItems.class);
		startActivity(aux);

	}
	public void onProveedor(View v) {

		Intent aux = new Intent(this, FProvedor.class);
		startActivity(aux);

	}
	public void onPrueba(View v) {

		Intent aux = new Intent(this, FCamara.class);
		startActivity(aux);

	}
	public void onPresupuesto(View v) {

		Intent aux = new Intent(this, FPresupuestos.class);
		startActivity(aux);

	}

}
