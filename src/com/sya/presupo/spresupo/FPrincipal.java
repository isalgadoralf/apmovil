package com.sya.presupo.spresupo;

import java.util.List;

import scz.bo.spresupo.R;

import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.model.Umedida;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class FPrincipal extends Activity {
	EditText txid;
	EditText txav;
	EditText txdes;

	private ListView lista;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fprincipal);

		txid = (EditText) findViewById(R.id.etubicacion);
		txav = (EditText) findViewById(R.id.editText2);
		txdes = (EditText) findViewById(R.id.editText3);
		lista = (ListView) findViewById(R.id.lvprovedor);
		ManangerSQlite m = new ManangerSQlite(this);
		Umedida u = new Umedida();
		List li = m.getLista(u);
		Log.d("LISTAS", "" + li.size());
		ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(this,
				android.R.layout.simple_list_item_1, li);
		lista.setAdapter(adapter);

	}

	public void onGuardar(View v) {
		/*
		 * ManangerSQlite m = new ManangerSQlite(this); Umedida u = new
		 * Umedida(txav.getText().toString(), txdes.getText().toString(),
		 * Integer.parseInt(txid.getText().toString())); boolean b =
		 * m.guardar(u); Toast.makeText(getApplicationContext(),""+ b,
		 * Toast.LENGTH_SHORT).show();
		 */
		Intent aux = new Intent(this, Fitems.class);
		startActivity(aux);

	}

	public void onDialog(View v) {
		LayoutInflater inflater = getLayoutInflater();

		View dialoglayout = inflater.inflate(R.layout.dialogpresupuesto, null);

		final EditText etAsunto = (EditText) dialoglayout.findViewById(R.id.etubicacion);
		
		Button btnEnviarMail = (Button) dialoglayout.findViewById(R.id.btnaceptar);
		btnEnviarMail.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				String subject = etAsunto.getText().toString();
				

				Toast.makeText(getApplicationContext(),subject, Toast.LENGTH_SHORT).show();
				

			}
		});

		AlertDialog.Builder builder = new AlertDialog.Builder(FPrincipal.this);
		builder.setView(dialoglayout);
		builder.show();
	}
}
