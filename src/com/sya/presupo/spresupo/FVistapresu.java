package com.sya.presupo.spresupo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParserException;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sya.presupo.adaptadores.ListaPresuAdaptador;
import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.datos.MyPDF;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Cliente;
import com.sya.presupo.model.Deitpre;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Presupuesto;
import com.sya.presupo.util.MyMath;
import com.sya.presupo.wservices.WSConsultas;

import scz.bo.spresupo.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FVistapresu extends Activity {
	private ListView lista;

	private TextView tvtotal;
	private ArrayList<eItems> listp;
	private ListaPresuAdaptador lpa;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fvistapresu);
		cargarCabezera();
		tvtotal = (TextView) findViewById(R.id.fvptvtotal);
		lista = (ListView) findViewById(R.id.fvplista);
		listp = new ArrayList<eItems>();

		CargarLista();
		lpa = new ListaPresuAdaptador(this, listp);
		lista.setAdapter(lpa);

		actualizarTotal();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mvistapresupuesto, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.mvpactualizar:

			/*
			 * ActualizarTask actualizarTask = new
			 * ActualizarTask(Puente.presupuesto); actualizarTask.execute();
			 */
			//generarPDF();
		//	showPdfFile(NOMBRE_DOCUMENTO, this);
			MyPDF mp = new MyPDF(this);
			mp.generarPDF();
			mp.showPdfFile();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	

	

	
	

	private void cargarCabezera() {
		Presupuesto p = new Presupuesto();

		p.setPresupuestoID(Puente.presupuesto.getPresupuestoID());

		ManangerSQlite m = new ManangerSQlite(this);
		p = (Presupuesto) m.getObjectCampo(p, p.getClass().getSimpleName()
				+ "ID", p.getPresupuestoID());
		Cliente cc = new Cliente();
		cc.setClienteID(p.getClienteID());

		cc = (Cliente) m.getObjectCampo(cc, cc.getClass().getSimpleName()
				+ "ID", cc.getClienteID());
		TextView tvcliente = (TextView) findViewById(R.id.fvptvnombres);
		tvcliente.setText(cc.getNombres() + " " + cc.getApellidos());

		TextView tvdireccion = (TextView) findViewById(R.id.fvptvdireccion);
		tvdireccion.setText(p.getDescripcion());

		TextView tvfecha = (TextView) findViewById(R.id.fvptvfecha);
		tvfecha.setText(p.getFecha().toString());

		TextView tvubicacion = (TextView) findViewById(R.id.fvptvubicacion);
		tvubicacion.setText(p.getUbicacion());
	}

	

	private void CargarLista() {
		listp.clear();
		Deitpre detalle = new Deitpre();
		// detalle.setPresupuestoID(Puente.idPresupueto);
		ManangerSQlite m = new ManangerSQlite(this);
		ArrayList<Object> deta = m.getListaCampo(detalle, "presupuestoID",
				Puente.presupuesto.getPresupuestoID());
		for (int i = 0; i < deta.size(); i++) {
			detalle = (Deitpre) deta.get(i);
			Item it = new Item();
			it = (Item) m.getObjectCampo(it, "itemID", detalle.getItemID());
			eItems eit = new eItems(it.getItemID(), it.getDescripcion(),
					it.getPrecio(), "");
			eit.setCantidad(detalle.getCantidad());
			listp.add(eit);

		}

	}

	private void actualizarTotal() {
		ArrayList<eItems> pr = lpa.getList();
		String aux = "";
		Double total = 0.0;
		for (int i = 0; i < pr.size(); i++) {
			total = total + pr.get(i).getCantidad() * pr.get(i).getPrecio();
		}
		total = MyMath.Redondear(total);
		tvtotal.setText(Double.toString(total));
	}

	private class ActualizarTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mProgressDialog;
		private Presupuesto pre = null;

		private WSConsultas mWsBuscador;

		public ActualizarTask(Presupuesto pd) {
			mProgressDialog = new ProgressDialog(FVistapresu.this);
			mWsBuscador = new WSConsultas();
			pre = pd;

		}

		@Override
		protected void onPreExecute() {

			mProgressDialog.setMessage("Actualizando");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			String aux;

			try {
				aux = mWsBuscador.getPresupuesto(pre.getPresupuestoID());
				byte[] arreglo = android.util.Base64.decode(aux,
						android.util.Base64.DEFAULT);
				File tarjeta = Environment.getExternalStorageDirectory();
				String nombre = pre.getDescripcion() + ".pdf";
				File file = new File(tarjeta.getAbsolutePath(), nombre);

				FileOutputStream fos = new FileOutputStream(file);
				fos.write(arreglo);
				fos.close();

				Log.d("WEBSERFPRESU", "PRODUCTOS: " + aux);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		/*
		 * private ArrayList<Productos> wsToDatos(ArrayList<wservices.Productos>
		 * lista){ ArrayList<Productos> aux = new ArrayList<Productos>(); for
		 * (int i = 0; i < lista.size(); i++) { aux.add(parse(lista.get(i))); }
		 * return aux; } private Datos.Productos parse(wservices.Productos p){
		 * Datos.Productos aux = new
		 * Productos(p.getId(),p.getDescripcion(),p.getIdcategoria
		 * (),p.getFechaactualizacion(),p.getPrecio(),null); return aux;
		 * 
		 * }
		 */
		@Override
		protected void onPostExecute(Void result) {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}

		}

	}

}
