package com.sya.presupo.spresupo;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.xmlpull.v1.XmlPullParserException;

import com.sya.presupo.adaptadores.ListaPresuAdaptador;
import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Cliente;
import com.sya.presupo.model.Deitpre;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Presupuesto;
import com.sya.presupo.model.Usuario;
import com.sya.presupo.util.MyMath;
import com.sya.presupo.wservices.WSConsultas;





import scz.bo.spresupo.R;





import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FRealizarpre extends Activity {
	private static final String TAG = FRealizarpre.class.getSimpleName();

	private EditText cantidad;

	private double total;
	private ListView lista;

	private TextView tvtotal;
	private ArrayList<eItems> listp;
	private ListaPresuAdaptador lpa;
	private int nitCliente;

	private AdapterView<?> actual;
	private int indiceLista;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_frealizarpre);
		lista = (ListView) findViewById(R.id.lvPedido);

		listp = new ArrayList<eItems>();
		
		ArrayList<eItems> lis = new ArrayList<eItems>();
		tvtotal = (TextView) findViewById(R.id.tvrpTotal);
		cantidad = (EditText) findViewById(R.id.tvrpObservacion);
		///------- Puente Items
		//String[] liss = getIntent().getExtras().getStringArray("id");
		
		//nitCliente = getIntent().getExtras().getInt("nitCliente");

		CargarLista();
		lpa = new ListaPresuAdaptador(this, listp);
		lista.setAdapter(lpa);
		lista.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				actual = arg0;
				indiceLista = arg2;

			}
		});
	
		actualizarTotal();

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mrealizarpre, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.datoscliente:
	        	//Intent aux = new Intent(this, Fitems.class);
	    		//startActivity(aux);
	        	onDialog();
	            return true;
	        case R.id.guardarpre:
	        	guardarPresu();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	public void CargarLista(){
		ArrayList<eItems> laux = Puente.lista;
		listp.clear();
		listp =  laux;
	}
	private void actualizarTotal(){
		ArrayList<eItems> pr = lpa.getList();
    	String aux = "";
    	total =0;
    	for (int i = 0; i < pr.size(); i++) {
    		total = total + pr.get(i).getCantidad()*pr.get(i).getPrecio();
		}
    	total = MyMath.Redondear(total);
    	tvtotal.setText(Double.toString(total));
	}
	public void AgregarValor(View v){
		String c =  cantidad.getText().toString();
		
		((ListaPresuAdaptador)actual.getAdapter()).list.get(indiceLista).setCantidad(Double.valueOf(c));
		((ListaPresuAdaptador)actual.getAdapter()).notifyDataSetChanged();
		
		actualizarTotal();
		
	}
	
	public void QuitarElementoLista(View v){
		((ListaPresuAdaptador)actual.getAdapter()).list.remove(indiceLista);
		((ListaPresuAdaptador)actual.getAdapter()).notifyDataSetChanged();
		actualizarTotal();
	}
	public void guardarPresu(){
	
		int idpre =  getIdpresupuesto();
		
		Date date = new Date();
		
		
		Presupuesto pre = new Presupuesto(Puente.descripcion, date, Puente.ubicacion, idpre, Puente.cliente.getClienteID(), 1);
		ManangerSQlite m = new ManangerSQlite(getApplicationContext());
		if (m.guardarID(pre)){
			try {
				pre =  (Presupuesto) m.getUltimoObject(pre);
				ArrayList<Deitpre> de = guardarDetalle(pre.getPresupuestoID());
				pre.set_detalle(de);
				ActualizarTask actualizarTask = new ActualizarTask(pre);
		    	actualizarTask.execute();
		    	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

		
		
		
	}
	private ArrayList<Deitpre> guardarDetalle( int idpre){
		ArrayList<eItems> pr = lpa.getList();
		ArrayList<Deitpre> de =  new ArrayList<Deitpre>();
		for (int i = 0; i < pr.size(); i++) {
				eItems it = pr.get(i);
				double total = it.getCantidad()*it.getPrecio();
				Deitpre di = new Deitpre(it.getCantidad(), total, idpre, it.getItmesID());
				ManangerSQlite m =  new ManangerSQlite(getApplicationContext());
				m.guardarID(di); 
				de.add(di);
		}
		return de;
	}
	private int getIdpresupuesto(){
		Presupuesto pre = new Presupuesto();
		ManangerSQlite m = new ManangerSQlite(getApplicationContext());
		try {
			int idpre = 0;
			pre  = (Presupuesto) m.getUltimoObject(pre);
			if(pre.getPresupuestoID() == null){
				return 1;
			}else{
				return pre.getPresupuestoID()+1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	public void onDialog() {
		LayoutInflater inflater = getLayoutInflater();
		ArrayList<Object> list = new ArrayList<Object>();
		Cliente c = new Cliente();
		ManangerSQlite m = new ManangerSQlite(getApplicationContext());
		list =  m.getLista(c);
		 
	    /** Declaring an ArrayAdapter to set items to ListView */
	    ArrayAdapter<Object> adapter;
	    adapter = new ArrayAdapter<Object>(this, android.R.layout.simple_spinner_item, list);
	    

		View dialoglayout = inflater.inflate(R.layout.dialogpresupuesto, null);
	    final Spinner spinner = (Spinner)dialoglayout.findViewById(R.id.spcliente);
	    spinner.setAdapter(adapter);
		final EditText etAsunto = (EditText) dialoglayout.findViewById(R.id.etubicacion);
		final EditText etdescr = (EditText) dialoglayout.findViewById(R.id.etdesicripcioin);
		
		Button btnEnviarMail = (Button) dialoglayout.findViewById(R.id.btnaceptar);
		btnEnviarMail.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Puente.cliente = (Cliente) spinner.getSelectedItem();
				Puente.ubicacion = etAsunto.getText().toString();
				Puente.descripcion = etdescr.getText().toString();

				//String subject = etAsunto.getText().toString();
				Cliente cc = (Cliente) spinner.getSelectedItem();
				String subject = etAsunto.getText().toString() + " " + cc.getApellidos()+ " "+etdescr.getText().toString();
				Toast.makeText(getApplicationContext(),subject, Toast.LENGTH_SHORT).show();
				

			}
		});

		AlertDialog.Builder builder = new AlertDialog.Builder(FRealizarpre.this);
		builder.setView(dialoglayout);
		builder.show();
	}
	private class ActualizarTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mProgressDialog;
		private Presupuesto pre= null;
	
		private WSConsultas mWsBuscador;

		public ActualizarTask(Presupuesto pd) {
			mProgressDialog = new ProgressDialog(
					FRealizarpre.this);
			mWsBuscador = new WSConsultas();
			pre = pd;
		
		}

		@Override
		protected void onPreExecute() {

			mProgressDialog.setMessage("Actualizando");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				
				
				String aux = mWsBuscador.guardarPresupo(pre);
		
			
				Log.d(TAG, "PRODUCTOS: " +aux);
				
				
							
			} catch (IOException e) {
				Log.e(TAG, "" + e.getMessage());
			} catch (XmlPullParserException e) {
				Log.e(TAG, "" + e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e(TAG, "" + e.getMessage());
				
			} 

			return null;
		}
	/*	private ArrayList<Productos> wsToDatos(ArrayList<wservices.Productos> lista){
			ArrayList<Productos> aux = new ArrayList<Productos>();
			for (int i = 0; i < lista.size(); i++) {
				aux.add(parse(lista.get(i)));
			}
			return aux;
		}
		private Datos.Productos parse(wservices.Productos  p){
			 Datos.Productos aux = new Productos(p.getId(),p.getDescripcion(),p.getIdcategoria(),p.getFechaactualizacion(),p.getPrecio(),null);
			 return aux;
			
		}*/
		@Override
		protected void onPostExecute(Void result) {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}

		  
		}
	

	}
}
