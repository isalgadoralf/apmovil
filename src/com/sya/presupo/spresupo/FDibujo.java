package com.sya.presupo.spresupo;

import java.util.ArrayList;

import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Cliente;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Umedida;
import com.sya.presupo.util.MyMath;

import scz.bo.spresupo.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Path;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class FDibujo extends Activity {
	MyCanvas canvas;
	public String falto;
	public String fancho;
	final Context context = this;
	public eItems e;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_fdibujo);
		int w = this.getWindowManager().getDefaultDisplay().getWidth();
		int h = this.getWindowManager().getDefaultDisplay().getHeight();

		canvas = new MyCanvas(this, w, h);
		setContentView(canvas);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mdibujo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.mdborrar:
			// Intent aux = new Intent(this, Fitems.class);
			// startActivity(aux);
			// onDialog();
			canvas.Nuevo();
			
		
			return true;

		case R.id.mdconfigurar:
			dialogo();
			return true;
		case R.id.mditems:
			dialogoItems();
			return true;
		case R.id.mdcalcualr:
			double c = 0.0;
			this.canvas.accion  =0;
			if(canvas.it.getTipo()  == 0){
				double lineal = canvas.calcularMuro();
				 c =  MyMath.Redondear( lineal*canvas.altura);
			}
			if(canvas.it.getTipo()  == 2){
				c = MyMath.calculoArea(canvas.puntos);
			}
			if(canvas.it.getTipo()  == 1){
				c = canvas.puntos.size();
			}
			
			
			
			
			canvas.it.setCantidad(c);
			canvas.lsitem.add(canvas.it);
			canvas.clearCanvas();
			//onRelizarPres();
		
			Toast.makeText(context, ""+c, Toast.LENGTH_SHORT).show();
			return true;
		case R.id.mdrepresu:
			onRelizarPres();
		case R.id.mdModifcar:
			 canvas.accion = 1;
			 Toast.makeText(context, "1 MODIICAR"+canvas.accion, Toast.LENGTH_SHORT).show();
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	public void onRelizarPres(){
		//ArrayList<eItems> ls = new ArrayList<eItems>();
		
		Puente.lista  = canvas.lsitem;
    	Intent aux = new Intent(this, FRealizarpre.class);
		startActivity(aux);
		
	}

	public void dialogo() {
		// get prompts.xml view
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.dlgaltura, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		final EditText altura = (EditText) promptsView
				.findViewById(R.id.ddlgaltura);
		final EditText ancho = (EditText) promptsView
				.findViewById(R.id.ddlgancho);

		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// get user input and set it to result
						// edit text
						falto = altura.getText().toString();
						fancho = ancho.getText().toString();
						Toast.makeText(getApplicationContext(), falto + fancho,
								Toast.LENGTH_SHORT).show();
						canvas.equi = Double.parseDouble(fancho);
						canvas.altura = Double.parseDouble(falto);
						canvas.calexquivalencia();
						canvas.clearCanvas();
						canvas.Nuevo();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		

	}
	public void dialogoItems() {
		// get prompts.xml view
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.dlgitems, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		ArrayList<Object> list = new ArrayList<Object>();
		Item c = new Item();
		ManangerSQlite m = new ManangerSQlite(getApplicationContext());
		list =  m.getLista(c);
		 
	    /** Declaring an ArrayAdapter to set items to ListView */
	    ArrayAdapter<Object> adapter;
	    adapter = new ArrayAdapter<Object>(this, android.R.layout.simple_spinner_item, list);
	    final Spinner spinner = (Spinner)promptsView.findViewById(R.id.spitems);
	    spinner.setAdapter(adapter);
	    
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// get user input and set it to result
						// edit text
						Item e = (Item) spinner.getSelectedItem();
						
						eItems it = new eItems(e.getItemID(), e.getDescripcion(), e.getPrecio(), "m2");
						it.setColor(e.getColor());
						it.setTipo(e.getTipo());
					//	canvas.lsitem.add(it);
						canvas.it =  it;
						String c = "#"+it.getColor();
						if(it.getTipo() == 2){
							canvas.Nuevo();
						}
						
						canvas.paint.setColor(Color.parseColor(c));
						
						Toast.makeText(context, it.getDescripcion(), Toast.LENGTH_SHORT).show();
						
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		

	}
}
