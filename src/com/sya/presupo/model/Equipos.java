package com.sya.presupo.model;
public class Equipos {

    private String descripcion;
    private Double precio;
    private Integer equiposID;
    private Integer umedidaID;

    public Equipos( ) { 
      }
    public Equipos(String descripcion,Double precio,Integer equiposID,Integer umedidaID){
        this.descripcion = descripcion;
        this.precio = precio;
        this.equiposID = equiposID;
        this.umedidaID = umedidaID;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getEquiposID() {
        return equiposID;
    }

    public void setEquiposID(Integer equiposID) {
        this.equiposID = equiposID;
    }

    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }

    @Override
    public String toString() {
        return descripcion;
    }
    

}