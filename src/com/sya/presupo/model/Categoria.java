package com.sya.presupo.model;

public class Categoria {

    private String descripcionn;
    private Integer categoriaID;

    public Categoria( ) { 
      }
    public Categoria(String descripcionn,Integer categoriaID){
        this.descripcionn = descripcionn;
        this.categoriaID = categoriaID;
    }
    public String getDescripcionn() {
        return descripcionn;
    }

    public void setDescripcionn(String descripcionn) {
        this.descripcionn = descripcionn;
    }

    public Integer getCategoriaID() {
        return categoriaID;
    }

    public void setCategoriaID(Integer categoriaID) {
        this.categoriaID = categoriaID;
    }

    @Override
    public String toString() {
        return descripcionn; //To change body of generated methods, choose Tools | Templates.
    }

   
}