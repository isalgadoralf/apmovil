package com.sya.presupo.estructuras;

import java.util.ArrayList;

public class Linea {
	private int radio;
	private ArrayList<Point> puntos;
	
	
	public Linea() {
		// TODO Auto-generated constructor stub
		puntos  = new ArrayList<Point>();
		radio = 50;
	}
	public void setPuntos(ArrayList<Point> puntos) {
		this.puntos = puntos;
	}
	public ArrayList<Point> getPuntos() {
		return puntos;
	}
	public void setRadio(int radio) {
		this.radio = radio;
	}
	public int getRadio() {
		return radio;
	}
	
	

}
