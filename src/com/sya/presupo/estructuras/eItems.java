/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sya.presupo.estructuras;

/**
 *
 * @author Rafael
 */
public class eItems {

    private Integer itmesID;
    private String Descripcion;
    private Double cantidad=1.0;
    private Double precio;
    private String umedida;
    private String color;
    private Integer tipo;
    
    private boolean checable = false;

    public eItems() {
    }

    public eItems(Integer itmesID, String Descripcion, Double precio, String umedida) {
        this.itmesID = itmesID;
        this.Descripcion = Descripcion;
        this.precio = precio;
        this.umedida = umedida;
        
    }

    public void setItmesID(Integer itmesID) {
        this.itmesID = itmesID;
    }

    public Integer getItmesID() {
        return itmesID;
    }

   
    

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }
    public void setChecable(boolean checable) {
		this.checable = checable;
	}
    public boolean isChecable() {
		return checable;
	}
    public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
    public Double getCantidad() {
		return cantidad;
	}
    public void setColor(String color) {
		this.color = color;
	}
    public String getColor() {
		return color;
	}
    public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
    public Integer getTipo() {
		return tipo;
	}
    
}
